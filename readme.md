# Getting started with New-Component.sh


## About: 
Pretty much just a quick way to create scripts in their requisite folders for an MVC approach to Unity.

## Running: 

It's pretty easy, just plug this into the terminal
`./scripts/new-component.sh [component-name] [folder-name (optional)]`
this'll create a new component in the requested folder (else it'll create it under Assets/Scripts/Components)

## TODO: 

<!-- * Figure out a good way to have this copied across projects and in source control. Maybe another submodule? -->
* Fill out the template files some more, get a really standardised approach to building stuff.