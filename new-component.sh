#!/bin/bash
name="$1"
folder="$2"

if [ -z "$name" ]; then
  echo "No component name supplied."
  echo "Example: ./new-component MyComponent"
  exit 1
fi

templates_folder=./scripts/templates
placeholder=__COMPONENT_NAME__
components_folder=./Assets/_stoppable/scripts/components/$folder
new_component_folder="$components_folder/$name"

Data="Data"
View="View"
Controller="Controller"
ControllerTests="ControllerTests"
ViewTests="ViewTests"
Test="Tests"

model_name="$name$Data"
view_name="$name$View"
controller_name="$name$Controller"
test_name="$name$Test"
edittest_name="$name$ControllerTests"
playtest_name="$name$ViewTests"

model_folder="data/"
view_folder="view/"
controller_folder="controller/"
test_folder="[test]/"

if [ -d "$new_component_folder" ]; then
  echo "Component $name already exists."
  exit 1
fi

mkdir -p "$new_component_folder/$model_folder"
mkdir -p "$new_component_folder/$view_folder"
mkdir -p "$new_component_folder/$controller_folder"
mkdir -p "$new_component_folder/$test_folder"


sed "s/$placeholder/$model_name/" "$templates_folder/Data.cs" > "$new_component_folder/$model_folder/$model_name.cs"
sed "s/$placeholder/$view_name/" "$templates_folder/View.cs" > "$new_component_folder/$view_folder/$view_name.cs"
sed "s/$placeholder/$controller_name/" "$templates_folder/Controller.cs" > "$new_component_folder/$controller_folder/$controller_name.cs"

# test files
sed "s/$placeholder/$edittest_name/" "$templates_folder/ControllerTests.cs" > "$new_component_folder/$test_folder/$edittest_name.cs"
sed "s/$placeholder/$playtest_name/" "$templates_folder/ViewTests.cs" > "$new_component_folder/$test_folder/$playtest_name.cs"

sed "s/$placeholder/$test_name/" "$templates_folder/Tests.asmdef" > "$new_component_folder/$test_folder/$test_name.asmdef"

echo "Component $name created successfully."