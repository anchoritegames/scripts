#!/bin/bash
name="$1"
folder="$2"

if [ -z "$name" ]; then
  echo "No component name supplied."
  echo "Example: ./new-component MyComponent"
  exit 1
fi

templates_folder=./scripts/templates/ui-toolkit/
placeholder=__COMPONENT_NAME__
# pathPlaceholder=__COMPONENT_NAME__
components_folder=./Assets/_stoppable/scripts/ui/$folder
new_component_folder="$components_folder/$name"



Style="Style"
View="View"
Controller="Controller"
ControllerTests="ControllerTests"
Test="Tests"

style_name="$name$Style"
view_name="$name$View"
controller_name="$name$Controller"
test_name="$name$Test"
edittest_name="$name$ControllerTests"

test_folder="[test]/"

if [ -d "$new_component_folder" ]; then
  echo "Component $name already exists."
  exit 1
fi

mkdir -p "$new_component_folder/$model_folder"
mkdir -p "$new_component_folder/$view_folder"
mkdir -p "$new_component_folder/$controller_folder"
mkdir -p "$new_component_folder/$test_folder"


sed "s/$placeholder/$style_name/" "$templates_folder/ui-toolkit-styles.uss" > "$new_component_folder/$style_name.uss"

sed "s/$placeholder/$view_name/" "$templates_folder/ui-toolkit-view.uxml" > "$new_component_folder/$view_name.uxml"

sed "s/$placeholder/$controller_name/" "$templates_folder/ui-toolkit-controller.cs" > "$new_component_folder/$controller_name.cs"

# test files
sed "s/$placeholder/$edittest_name/" "$templates_folder/ui-toolkit-controller-tests.cs" > "$new_component_folder/$test_folder/$edittest_name.cs"

sed "s/$placeholder/$test_name/" "$templates_folder/Tests.asmdef" > "$new_component_folder/$test_folder/$test_name.asmdef"



## Edit view file
# Replace directory path

# Replace styles path
pathPlaceholder=__STYLE_PATH__
styleFileName="balls"

sed -i '' "s/$pathPlaceholder/$styleFilename/" "$new_component_folder/$view_name.uxml" 


echo "Component $name created successfully."