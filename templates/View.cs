//  Created by Matt Purchase.
//  Copyright (c) 2021 Matt Purchase. All rights reserved.
using System;
using System.Collections.Generic;
using UnityEngine;


public class __COMPONENT_NAME__ : MonoBehaviour {
	// Properties
	[SerializeField] private ControllerName m_controller;

	// Initalisation Functions

	// Unity Callbacks
	public void Initialise() {
		m_controller.Initialise(this);
	}

	private void ShutDown() {
		Unsubscribe();
	}

	private void Unsubscribe() {

	}

	// Public Functions

	// Private Functions

}