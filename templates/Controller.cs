//  Created by Matt Purchase.
//  Copyright (c) 2021 Matt Purchase. All rights reserved.
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class __COMPONENT_NAME__ {
	// Properties
	public Data m_data;
	private View m_view;
	// Initalisation Functions

	public void Initialise() {
		CollectView();
	}

	private void ShutDown() {
		Unsubscribe();
	}

	private void Unsubscribe() {

	}

	private void CollectView() {

	}

	// Unity Callbacks

	// Public Functions

	// Private Functions

}